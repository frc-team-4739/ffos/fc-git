# FC Git

A series of Git hooks which handles FreeCAD files (.FCStd) which are just zipped XML. As well as an extension to use basic Git features from FreeCAD. Note that the listing of changes doesn't work properly.

# Acknowledgements
[XLSX Git - Katriel Friedman!](https://github.com/ckrf/xlsx-git)
