# FCGit, a workbench to integrate FreeCAD with Git version control
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from FreeCAD import GuiUp
import FreeCAD

from fcg_commands.Initialise import Initialise
from fcg_commands.Clone      import Clone
from fcg_commands.KeySetup   import KeySetup
from fcg_commands.Pull       import Pull
from fcg_commands.Push       import Push
from fcg_commands.Commit     import Commit
from fcg_commands.Branch     import Branch
from fcg_commands.Checkout   import Checkout
from fcg_commands.Sync       import Sync


if GuiUp:
    FreeCAD.Gui.addCommand("Initialise", Initialise())
    FreeCAD.Gui.addCommand("Clone"     , Clone     ())
    FreeCAD.Gui.addCommand("KeySetup"  , KeySetup  ())
    FreeCAD.Gui.addCommand("Pull"      , Pull      ())
    FreeCAD.Gui.addCommand("Push"      , Push      ())
    FreeCAD.Gui.addCommand("Commit"    , Commit    ())
    FreeCAD.Gui.addCommand("Branch"    , Branch    ())
    FreeCAD.Gui.addCommand("Checkout"  , Checkout  ())
    FreeCAD.Gui.addCommand("Sync"      , Sync      ())
