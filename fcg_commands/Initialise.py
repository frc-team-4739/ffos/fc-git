# FCGit, a workbench to integrate FreeCAD with Git version control
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from PySide import QtGui
from shutil import copyfile

from fcg_commands.Utils import *


class Initialise:
    def GetResources(self):
        return {
            "Pixmap": icon_dir + "Initialise.svg",
            "MenuText": "Initialise",
            "ToolTip": "Initialise a Git repository"
        }

    def IsActive(self):
        return True

    def Activated(self):
        fix_environ()
        invalid = False
        while True:
            loc = QtGui.QFileDialog.getExistingDirectory(
                None,
                ("ERROR: " if invalid else "") + "Select Repository Location",
                get_pwd()
            )
            if loc == "":
                return
            if Popen(["git", "init", loc]).wait() == 0:
                chdir(loc)
                Popen(["git", "commit", "--allow-empty", "-m", "Initial Commit"]).wait()
                init_repo()
                copyfile(source_dir + "/ignore", ".gitignore")
                return
            invalid = True
