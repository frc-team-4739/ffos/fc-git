# FCGit, a workbench to integrate FreeCAD with Git version control
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from PySide import QtGui

from fcg_commands.Utils import *


class Commit:
    def GetResources(self):
        return {
            "Pixmap": icon_dir + "Commit.svg",
            "MenuText": "Commit",
            "ToolTip": "Commit the changes with a message"
        }

    def IsActive(self):
        return is_repo()

    def Activated(self):
        fix_environ()
        w = QtGui.QDialog()
        w.setWindowTitle("Enter Commit Message")
        w.resize(320, 240)
        tb = QtGui.QPlainTextEdit(w)
        tb.resize(320, 220)

        def ok():
            set_pwd()
            Popen(["git", "add", "."]).wait()
            Popen(["git", "commit", "-m", tb.toPlainText()]).wait()
            w.close()

        def cancel():
            w.close()

        bt = QtGui.QDialogButtonBox(w)
        bt.setStandardButtons(QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel)
        bt.button(QtGui.QDialogButtonBox.Ok).clicked.connect(ok)
        bt.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(cancel)
        bt.move(153.3, 223.3)

        w.show()
        pass
