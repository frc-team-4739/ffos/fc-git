# FCGit, a workbench to integrate FreeCAD with Git version control
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from fcg_commands.Utils import *


class Push:
    def GetResources(self):
        return {
            "Pixmap": icon_dir + "Push.svg",
            "MenuText": "Push",
            "ToolTip": "Push changes to the remote repository"
        }

    def IsActive(self):
        return is_repo()

    def Activated(self):
        fix_environ()
        set_pwd()
        Popen(["git", "push", "-u"])
