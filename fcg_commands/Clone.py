# FCGit, a workbench to integrate FreeCAD with Git version control
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from PySide import QtGui

from fcg_commands.Utils import *


class Clone:
    def GetResources(self):
        return {
            "Pixmap": icon_dir + "Clone.svg",
            "MenuText": "Clone",
            "ToolTip": "Clone a Git repository from a URI"
        }

    def IsActive(self):
        return True

    def Activated(self):
        fix_environ()
        loc = QtGui.QFileDialog.getExistingDirectory(
            None,
            "Select Clone Location",
        )
        if loc == "":
            return
        invalid = False
        while True:
            uri, ok = QtGui.QInputDialog.getText(
                None,
                "Clone Repository", ("ERROR: Could not clone\n\n" if invalid else "") + "Enter URI"
            )
            if not ok:
                return
            if Popen(["git", "clone", uri, loc]).wait() == 0:
                chdir(loc)
                init_repo()
                Popen(["git", "checkout", "HEAD"])
                Popen(["git", "recalc"])
                return
            invalid = True
