# FCGit, a workbench to integrate FreeCAD with Git version control
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from PySide import QtGui

from fcg_commands.Utils import *


class KeySetup:
    def GetResources(self):
        return {
            "Pixmap": icon_dir + "Key.svg",
            "MenuText": "Key Setup",
            "ToolTip": "Create or view a SSH key to connect to Git"
        }

    def IsActive(self):
        return True

    def Activated(self):
        fix_environ()
        if not Path(str(Path.home()) + "/.ssh/id_rsa").exists():
            reply = QtGui.QMessageBox.question(
                None,
                "Create SSH Key",
                "No SSH key detected. Should a new one be created?",
                QtGui.QMessageBox.Yes,
                QtGui.QMessageBox.No
            )

            if reply == QtGui.QMessageBox.No:
                return

            Popen(["ssh-keygen", "-b", "2048", "-t", "rsa", "-f", str(Path.home()) + "/.ssh/id_rsa", "-q", "-N", ""]).wait()
        msg = QtGui.QMessageBox()
        msg.setIcon(QtGui.QMessageBox.NoIcon)
        msg.setText(
            "Copy the following public key to your Git provider (eg. GitHub or GitLab). You "
            "do not have to keep this key private. The key can be viewed under details."
        )
        key = ""
        with open(str(Path.home()) + "/.ssh/id_rsa.pub") as kf:
            key = kf.read()
        msg.setDetailedText(key)

        msg.setWindowTitle("SSH Public Key")
        msg.addButton(QtGui.QMessageBox.Ok)

        def copy():
            cb = QtGui.QApplication.clipboard()
            cb.clear(mode=cb.Clipboard)
            cb.setText(key, mode=cb.Clipboard)
            msg.close()

        btn = msg.addButton("Copy", QtGui.QMessageBox.YesRole)
        btn.clicked.disconnect()
        btn.clicked.connect(copy)
        msg.show()

        reply = QtGui.QMessageBox.question(
            None,
            "Add Repo Keys",
            "Do you want to add keys for common Git repositories (eg. GitHub)?",
            QtGui.QMessageBox.Yes,
            QtGui.QMessageBox.No
        )

        if reply == QtGui.QMessageBox.No:
            return

        with open(str(Path.home()) + "/.ssh/known_hosts") as hf:
            with open(source_dir + "repokeys") as rk:
                hf.write(rk.read())
