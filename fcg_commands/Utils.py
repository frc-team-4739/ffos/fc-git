# FCGit, a workbench to integrate FreeCAD with Git version control
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import FreeCAD
from re import sub
from pathlib import Path
from os import chdir, symlink, mkdir, environ
from shutil import rmtree
from subprocess import Popen, check_output
from typing import List


source_dir = str(Path(__file__).parent.parent)
icon_dir = source_dir + "/icons/"


def fix_environ():
    if "PYTHONHOME" in environ:
        del environ["PYTHONHOME"]


def get_pwd() -> str:
    doc = FreeCAD.ActiveDocument
    if doc is None:
        return str(Path.home())
    return str(Path(doc.FileName).parent)


def set_pwd():
    chdir(get_pwd())


def is_repo() -> bool:
    return Popen(["git", "-C", get_pwd(), "rev-parse"]).wait() == 0


def get_branches() -> List[str]:
    bs = set(map(
        lambda x: sub(".*/(?:origin|heads)/(?:HEAD)?", "", x),
        check_output(["git", "for-each-ref", "--format=%(refname)"]).decode("utf-8").strip().splitlines()
    ))
    if "" in bs:
        bs.remove("")
    return list(bs)


def init_repo():
    rmtree(".git/hooks")
    symlink(source_dir + "/hooks", ".git/hooks")
    with open(".git/config", "a") as config:
        config.write(
            "[include]\n"
            + "\tpath=" + source_dir + "/gitconfig\n"
        )

