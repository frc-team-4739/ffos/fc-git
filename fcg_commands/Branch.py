# FCGit, a workbench to integrate FreeCAD with Git version control
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from PySide import QtGui

from fcg_commands.Utils import *


class Branch:
    def GetResources(self):
        return {
            "Pixmap": icon_dir + "Branch.svg",
            "MenuText": "Branch",
            "ToolTip": "Create and switch the to a new branch"
        }

    def IsActive(self):
        return is_repo()

    def Activated(self):
        fix_environ()
        set_pwd()
        invalid = False
        exists = False
        while True:
            branch, ok = QtGui.QInputDialog.getText(
                None,
                "Create New Branch",
                ("ERROR: Existing Branch Name\n\n" if exists else ("ERROR: Invalid Branch Name\n\n" if invalid else "")) +
                "Enter Branch Name"
            )
            if not ok:
                return
            if branch.strip() in get_branches():
                exists = True
                continue
            if Popen(["git", "check-ref-format", "--branch", branch]).wait() == 0:
                Popen(["git", "branch", branch]).wait()
                Popen(["git", "checkout", branch]).wait()
                return
            exists = False
            invalid = True
