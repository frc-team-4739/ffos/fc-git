#!/usr/bin/env python3

# FC Git, a series of hooks which can handle FreeCAD files
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from serialization import serialize
from utils import git
from re import sub


print("\n=== Start of pre-commit FCStd processing ===")

for file in git(["diff", "--name-only", "--cached", "--", "*/Document.xml"]).splitlines():
    # Don't care about deleted files
    print("Processing {}... ".format(file), end="")
    git(["reset", file])
    git(["add"] + serialize(file))
    print("done")

for file in git(["diff", "--name-only", "--cached", "--", "*.xml"]).splitlines():
    try:
        print("Processing {}...".format(file), end="")
        git(["reset", file])
        with open(file, "r") as xf:
            xt = xf.read()
        with open(file, "w") as xf:
            xf.write(sub(r"<!\[CDATA\[\s*CASCADE[\s\S]*?\]\]>", "", xt))
            pass
        git(["add", file])
        with open(file, "w") as xf:
            xf.write(xt)
        print("done")
    except:
        pass

print("=== End of pre-commit FCStd processing ===\n")
