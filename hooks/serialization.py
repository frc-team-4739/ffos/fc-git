#!/usr/bin/env python3

# FC Git, a series of hooks which can handle FreeCAD files
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from typing import List
from re import findall, search
from os.path import dirname, join


def serialize(file: str) -> List[str]:
    dd = dirname(file)
    with open(file, "r") as df:
        dt = df.read()

        pp = join(dd, "Properties.xml")
        with open(pp, "w") as pf:
            pf.writelines(map(
                lambda x: x[2:] + "\n",
                search(r"  <Properties.*?>[\s\S]*?</Properties>", dt).group(0).splitlines()
            ))
        op = join(dd, "Objects")
        with open(op, "w") as of:
            of.writelines(map(
                lambda x: x + "\n",
                findall(r"<Object\s+.*?/>", dt)
            ))
    return [pp, op]


def deserialize(file: str):
    dd = dirname(file)
    with open(join(dd, "Document.xml"), "w") as df:
        df.write(
            "<?xml version='1.0' encoding='utf-8'?>\n" +
            "<!--\n" +
            " FreeCAD Document, see http://www.freecadweb.org for more information...\n" +
            "-->\n" +
            "<Document SchemaVersion=\"4\" ProgramVersion=\"2020.1230R23076 +3055 (Git)\" FileVersion=\"2\" StringHasher=\"1\">\n" +
            "<StringHasher saveall=\"0\" threshold=\"0\" count=\"0\"></StringHasher>\n"
        )
        with open(join(dd, "Properties.xml"), "r") as pf:
            df.writelines(map(
                lambda x: "  {}".format(x),
                pf.readlines()
            ))

        with open(join(dd, "Objects"), "r") as of:
            ls = of.readlines()
            df.write("  <Objects Count=\"{}\" Dependencies=\"1\">\n".format(len(ls)))
            df.writelines(map(
                lambda x: "    " + x,
                ls
            ))

        df.write("  </Objects>\n")
        df.write("  <ObjectData Count=\"0\"></ObjectData>\n")
        df.write("</Document>\n")
