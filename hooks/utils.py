#!/usr/bin/env python3

# FC Git, a series of hooks which can handle FreeCAD files
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from typing import Set, Tuple, Dict, List
from subprocess import PIPE, Popen
from collections import OrderedDict
from os import getcwd, chdir, environ
from contextlib import contextmanager
from glob import glob
from re import search
from os.path import abspath
from pathlib import Path

import re


def git(args):
    return Popen(
        ['git'] + args,
        stdout=PIPE
    ).stdout.read().decode('utf-8', 'replace').strip()


@contextmanager
def cwd(path):
    cwd = getcwd()
    chdir(path)
    yield
    chdir(cwd)


def get_deps() -> Tuple[Set[Tuple[str, str]], Dict[str, Set[str]]]:
    deps = set()
    ds = {}
    fe = re.compile(r"Link\d*\.xml*")
    for link in filter(fe.search, glob("**/*", recursive=True)):
        with open(link) as lf:
            pp = abspath(str(Path(link).parent))
            with cwd(str(Path(pp).parent)):
                rp = abspath(search("<XLink file=\"(.*?)\"", lf.read()).group(1))
            deps.add((pp, rp))
            if rp in ds:
                ds[rp].add(pp)
            else:
                ds[rp] = {pp}
    return deps, ds


def topological_sort(nodes: Set[str], links: Set[Tuple[str, str]]) -> OrderedDict[str, bool]:
    l = OrderedDict()
    while True:
        s = nodes - set(map(lambda x: x[0], links))
        if len(s) == 0:
            return l
        n = s.pop()
        nodes.remove(n)
        l[n] = False
        links = list(filter(lambda x: x[1] != n, links))


def get_to_recalculate(files: List[str]) -> List[str]:
    deps, ds = get_deps()
    l = topological_sort(
        set(map(lambda x: abspath(str(Path(x).parent)), glob("**/Properties.xml", recursive=True))),
        deps
    )

    for f in files:
        l[abspath(str(Path(f).parent))] = True
    for doc in l:
        if l[doc] and doc in ds:
            for req in ds[doc]:
                l[req] = True

    return list(filter(lambda x: x[0], l))


def recalculate(files: List[str]):
    Popen([
        environ["FREECAD_PATH"],
        "-c",
        # language=python
        """for dp in {}:
    try:
        print("Recalculating " + dp)
        d = App.openDocument(dp)
        os = d.Objects
        while len(os) != 0:
            os = list(filter(lambda x: not x.recompute(), os))
        d.save()
    except:
        pass""".format(str(get_to_recalculate(files)))
    ]).wait()
