# FCGit, a workbench to integrate FreeCAD with Git version control
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


__title__ = "FCGit Workbench for FreeCAD"
__author__ = "Evan La Fontaine (FRC Team 4739, CTRL F5)"
__url__ = "http://frc4739.freeddns.org/home.html"


import FreeCAD
import FreeCADGui


class FCGit(Workbench):
    from fcg_commands.Utils import icon_dir
    Icon = icon_dir + "Checkout.svg"
    MenuText = "FCGit"
    ToolTip = "Integrate FreeCAD with Git version control"

    def GetClassName(self):
        return "Gui::PythonWorkbench"

    def Initialize(self):
        import FCGInit
        self.Remote = [
            "Initialise",
            "Clone",
            "KeySetup",
            "Pull",
            "Push",
            "Sync"
        ]
        self.Local = [
            "Commit",
            "Branch",
            "Checkout"
        ]
        self.appendToolbar("FCGit Remote", self.Remote)
        self.appendToolbar("FCGit Local", self.Local)

    def Activated(self):
        FreeCAD.Console.PrintMessage("FCGit workbench loaded\n")


FreeCADGui.addWorkbench(FCGit)

